"""
Created on 16 May 2018

@author: anthon
"""


from sklearn.metrics.regression import r2_score


class Template(object):
    """
    Template classe
    """

    def __init__(self, metric=r2_score):
        self.metric = metric
        self.name = "Template"

    def fit(self, X, T):
        """
         Predict targets from feature vectors.

         Parameters
         ----------
         X : numpy.ndarray, size (n_samples, n_features)
         Feature vector to predict targets of.

         T : numpy.ndarray, size (n_samples, n_target)
         Target associated with feature vector.
         Returns
         -------
         self
         """

        return self


    def predict(self, X):
        """
        Predict targets from feature vectors.

        Parameters
        ----------
        X : numpy.ndarray, size (n_samples, n_features)
        Feature vector to predict targets of.

        Returns
        -------
        that : numpy.ndarray, size (n_samples, n_targets)
        Matrix of predicted targets.
        """
        that = 0
        #  [...]
        # that = ...
        return that

    def score(self, X, T):
        """
        Scores feature vectors prediction vs. feature vector target

        Parameters
        ----------
        X : numpy.ndarray, size (n_samples, n_features)
        Feature vector to score the predict targets of.

        T : numpy.ndarray, size (n_samples, n_targets)
        Target vectors associated with x.

        Returns
        -------
        scores : numpy.ndarray (n_samples,)
        Scoring of the predictions of input feature vectors.
        """
        that = self.predict(X)
        score = self.metric(T, that)
        return score

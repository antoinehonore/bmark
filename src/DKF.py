"""
Created on 02 Oct 2018

@author: Antoine Honore, ahonore@pm.me
"""

import sys
from functools import partial
import numpy as np
import numba
from numba import jit
import sklearn
from sklearn.metrics.scorer import r2_score

from bmark.src.utils import normc, compute_nme


class DKF(object):
    def __init__(self, alphas=tuple((10**i for i in range(-6, 4))),
                 lmax=10, delta=50, nnodes=1000, metric=r2_score, verbose=False):

        self.leaky_relu_coef = 0.01
        self.decay = .9
        self.delta = 50
        self.alphas = alphas
        self.lmax = lmax
        self.delta = delta
        self.nnodes = nnodes
        self.metric = metric
        self.gamma = [0 for _ in range(self.lmax)]
        metric_str = str(metric)
        self.name = "DKF"
        self.verbose = verbose

        if "r2_score" in metric_str or "accuracy" in metric_str:
            self.choice_fun = np.argmax
        elif "nme" in metric_str:
            self.choice_fun = np.argmin
        else:
            print("unknown metric function. Exit.", metric)
            sys.exit(1)

        self.metric_name = metric_str.split(' ')[1]
        self.kjobs = 1
        self.train_data = None
        self.train_label = None
        self.train_perf = [0 for _ in range(self.lmax)]

        self.A = [None for _ in range(self.lmax)]
        self.y = [None for _ in range(self.lmax)]
        self.K = [None for _ in range(self.lmax)]
        self.tval_hat = np.zeros((self.lmax, len(self.alphas)))


    def improvement(self, old, new):
        if self.choice_fun == np.argmin:
            return new < old

        if self.choice_fun == np.argmax:
            return new > old


    def find_best_lambda(self, alpha, l=None, T=None, train_idx=None, val_idx=None):
        # that = Ktest.T @ (K + alpha * n * I)^(-1) @ T
        TEMP = self.solve_regression(self.K[l][train_idx, :][:, train_idx], T[train_idx, :], alpha)
        tval_hat = self.K[l][train_idx, :][:, val_idx].T @ TEMP
        del TEMP
        layer_val_perf = self.metric(T[val_idx, :], tval_hat)
        return layer_val_perf


    def fit(self, X, T, vperc=.15):
        N = X.shape[0]
        nval = int(round(N*vperc))
        permut = np.random.permutation(N)
        val_idx = permut[:nval]
        train_idx = permut[nval:]

        self.train_data = X
        self.train_label = T

        y_in = self.train_data

        K_in, gamma = self.kernel(self.train_data)
        self.gamma_init = gamma
        self.val_perf = np.zeros(self.lmax)
        self.val_perf[:] = np.nan

        self.train_perf = np.zeros_like(self.val_perf)

        for l in range(self.lmax):
            if self.verbose:
                print("layer:", l, "/", self.lmax)

            layer_val_perf = np.zeros(len(self.alphas))

            self.K[l] = K_in

            # Help function for lambda search
            fp = partial(self.find_best_lambda, l=l, T=T, train_idx=train_idx, val_idx=val_idx)


            # Grid search TODO: Parallelize
            for i in numba.prange(len(self.alphas)):
                layer_val_perf[i] = fp(self.alphas[i])


            ibest = self.choice_fun(layer_val_perf)
            self.val_perf[l] = layer_val_perf[ibest]
            self.alpha = self.alphas[ibest]


            #  Early stop condition and memory management
            if l <= 1 or self.improvement(self.val_perf[l-1], self.val_perf[l]):
                self.A[l] = np.random.randn(y_in.shape[1], self.nnodes + self.delta*l)

                y_in = self.l_relu(self.normalize(y_in @ self.A[l]))
                self.y[l] = np.copy(y_in)

                Ktmp, gamma = self.kernel(self.y[l])
                K_in = Ktmp + self.K[l]
                del Ktmp

                self.gamma[l] = gamma
            else:
                self.y[l-1], self.A[l-1], self.K[l] = None, None, None
                break

            # The kernel of layer l - 1 uses up space
            if l >= 1:
                self.K[l-1] = None

        if self.verbose:
            print('Done.')

        return self


    def predict(self, x):
        y_test = [None for _ in range(self.lmax)]
        y_in = x
        K_test = self.kernel(self.train_data, y=x, gamma_in=self.gamma_init)
        Lmax = -1

        for l in range(self.lmax):
            if self.A[l] is None:
                Lmax = max(0, l)
                break

            y_test[l] = self.l_relu(self.normalize(y_in @ self.A[l]))
            K_test = self.kernel(self.y[l], y=y_test[l], gamma_in=self.gamma[l]) + K_test
            y_in = y_test[l]

        return K_test.T @ self.solve_regression(self.K[Lmax], self.train_label, self.alpha)


    def score(self, x, t):
        that = self.predict(x)
        return self.metric(t, that)

    #   Kernel computation
    def kernel(self, x, y=None, gamma_in=None):
        if self.verbose:
            print("Kernel...")

        ## Below implements rbf kernel
        # Compute euclidian distances
        pairdist = sklearn.metrics.pairwise_distances(x, Y=y) ** 2

        if gamma_in is None:
            gamma_out = (pairdist.sum() / pairdist.shape[1] / pairdist.shape[0]) ** (-1)
            return np.exp(-gamma_out * pairdist), gamma_out
        else:
            return np.exp(-gamma_in * pairdist)

    #   Regression
    def solve_regression(self, K, T, alpha):
        n = K.shape[0]
        return cholesky_inv(K + alpha * n * np.eye(n)) @ T

        #  return admm_solver((K + n * alpha * np.eye(n)), T.T, alpha, .01, 100).T

    def normalize(self, x):
        return normc(x, axis=1)

    #  Leaky relu and inverse
    def l_relu(self, x):
        x[x < 0] = self.leaky_relu_coef*x[x < 0]
        return x

    def admm_solver(self, l, eps_o, mu, kmax):
        x = self.K[l]
        p, N = x.shape
        y = np.eye(x.shape)
        Q, _ = y.shape
        Lam = np.zeros((Q, p))
        xxt = np.matmul(x, x.transpose())
        yxt = np.matmul(y, x.transpose())
        tmp = np.linalg.inv(xxt + 1 / mu * np.identity(p))
        Z = Lam
        cost = []

        for _ in range(kmax):
            O = np.matmul(yxt + (1 / mu) * (Z + Lam), tmp)
            Z = O - Lam
            nZ = np.linalg.norm(Z, 'fro')
            if nZ > eps_o:
                Z = Z * (eps_o / nZ)
            Lam = Lam + Z - O
            cost.append(compute_nme(y, np.matmul(O, x)))
            if len(cost) > 2 and cost[-2] <= cost[-1]:
                break

        if len(cost) == kmax:
            print("\n\n:::::WARNING:::: admm_solver Did not converge.\n\n")
        return O


def jacobi(A, b, N=25, x=None):
    """Solves the equation Ax=b via the Jacobi iterative method."""
    # Create an initial guess if needed
    if x is None:
        x = np.zeros(len(A[0]))

    # Create a vector of the diagonal elements of A
    # and subtract them from A
    D = np.diag(A)
    R = A - np.diagflat(D)

    # Iterate for N times
    for i in range(N):
        x = (b - np.dot(R, x)) / D
    return x

def cholesky_inv(A):
    # A = L L^* = L L^T.conj();
    # problem: A x = I <=> L L^* x = I
    # solve for y: L y = I
    # solve for x: L^* x = y

    Id = np.eye(A.shape[0])
    L = np.linalg.cholesky(A)
    y = np.linalg.solve(L, Id)
    out = np.linalg.solve(np.conj(np.transpose(L)), y)
    return out


def gauss(A, b, n=100000):
    x = np.random.randn(A.shape[0], A.shape[1])
    L = np.tril(A)
    U = A - L
    Ai = np.linalg.inv(A)
    for i in range(n):
        x = np.linalg.inv(L) @ (b - U @ x)
        if i % 100 == 0:
            print(i, ((x-Ai)**2).mean())
    return x


if __name__ == "__main__":
    from sklearn.datasets import load_digits
    from tbox.ml.utils import compute_accuracy, makeOnehot
    digits = load_digits()
    n, d = digits.data.shape
    idx = np.random.permutation(n)
    test_perc = .3
    ntest = int(round(n * test_perc))
    X = digits.data[idx[ntest:], :]
    T = makeOnehot(digits.target[idx[ntest:], ...])
    Xt = digits.data[idx[:ntest], :]
    Tt = makeOnehot(digits.target[idx[:ntest], ...])

    model = DKF()
    model.fit(X, T)

    print(compute_accuracy(T.T, model.predict(X).T),
          compute_accuracy(Tt.T, model.predict(Xt).T) )

    print('')
"""
Created on 20 Mar 2018

@author: anthon
"""

import numpy as np
import sys, os
import sklearn
import scipy.io
from tbox.utils import pkload

def load_dataset(fname='Vowel'):
    print("In load dataset", fname)
    test_perc = 1 / 3
    matfiles = os.path.join("data", "mat_files")

    if "emp-age" in fname:
        types = ["AR", "moments", "demos"]
        type = [x for x in types if x in fname]
        assert(len(type) == 1)
        type = type[0]

        print("matched", os.path.join("data", fname+".pkl"))
        data, = pkload(os.path.join("data",
                                    fname.replace("-"+type, "") +".pkl"))

        X = data[type]["X"]
        Y = data["ages"]["X"]

        healthy_frames = (data["time-to-sepsis"]["X"] == -80).reshape(-1)
        healthy_gid = np.unique(data["gids"][healthy_frames])

        healthy_tot = healthy_gid.shape[0]
        n_test_pat = int(round(test_perc*healthy_tot))
        test_nums = np.random.randint(0, healthy_tot, size=n_test_pat)
        test_bool = np.in1d(data["gids"], healthy_gid[test_nums])
        train_bool = ~test_bool
        return X[train_bool].T, Y[train_bool].T, X[test_bool].T, Y[test_bool].T


    if ".mat" in fname:
        fname = fname + '.mat'
        mat = scipy.io.loadmat(os.path.realpath(os.path.join("data", "mat_files", fname)))

    if "epilep" in fname:
        data = np.loadtxt(os.path.realpath(fname + ".csv"))

        ntest = int(round((test_perc * data.shape[0])))

        y = data[:, -1]
        Y = np.zeros((y.shape[0], np.unique(y).shape[0]))
        Y[np.arange(y.shape[0]), y.astype(int) - 1] = 1

        return data[ntest:, :-1].T, Y[ntest:, :].T, data[:ntest, :-1].T, Y[:ntest, :].T


    if "Vowel.mat" in fname:
        N = mat["featureMat"].shape[1]
        ntrain = 48 * 11
        ntest = 42 * 11
        idx = np.arange(N)
        X_ = np.take(mat["featureMat"], idx[:ntrain], axis=1)
        T_ = np.take(mat["labelMat"], idx[:ntrain], axis=1)
        Xt = np.take(mat["featureMat"], idx[ntrain:], axis=1)
        Tt = np.take(mat["labelMat"], idx[ntrain:], axis=1)
        return X_, T_, Xt, Tt

    if "ExtendedYaleB.mat" in fname:
        return div_dataset(mat["featureMat"], mat["labelMat"], test_perc=test_perc, saxis=1)

    if "AR.mat" in fname:
        return div_dataset(mat["featureMat"], mat["labelMat"], test_perc=test_perc, saxis=1)

    if "CIFAR-10.mat" in fname:
        return mat['train_x'], mat['train_y'], mat['test_x'], mat['test_y']

    if "CIFAR-100.mat" in fname:
        return mat['train_x'], mat['train_y_fine'], mat['test_x'], mat['test_y_fine']

    if "Caltech101.mat" in fname:
        return div_dataset(mat["featureMat"], mat["labelMat"], test_perc=test_perc, saxis=1)

    if "Satimage.mat" in fname:
        return mat['train_x'], mat['train_y'], mat['test_x'], mat['test_y']

    if "Pyrim.mat" in fname:
        Test_num = 25
        N = mat["DataMat"].shape[0]

        featureMat = mat["DataMat"][:, 1:]
        targetMat = mat["DataMat"][:, 0].reshape(-1, 1)
        X, T, Xt, Tt = div_dataset(featureMat, targetMat, test_perc=Test_num/N, saxis=0)
        return X.T, T.reshape(-1, 1).T, Xt.T, Tt.reshape(-1, 1).T

    if "Bodyfat.mat" in fname:
        Test_num = 84
        N, d = mat["DataMat"].shape
        featureMat = mat["DataMat"][:, np.array([0]+list(range(2, d)))]
        targetMat = mat["DataMat"][:, 1].reshape(-1, 1)
        X, T, Xt, Tt = div_dataset(featureMat, targetMat, test_perc=Test_num / N, saxis=0)
        return X.T, T.reshape(-1, 1).T, Xt.T, Tt.reshape(-1, 1).T

    if "Balloon.mat" in fname:
        Test_num = 667
        N, d = mat["DataMat"].shape
        featureMat = mat["DataMat"][:, 0].reshape(-1, 1)
        targetMat = mat["DataMat"][:, 1].reshape(-1, 1)
        X, T, Xt, Tt = div_dataset(featureMat, targetMat, test_perc=Test_num / N, saxis=0)
        return X.T, T.reshape(-1, 1).T, Xt.T, Tt.reshape(-1, 1).T

    if "Abalone.mat" in fname:
        Test_num = 1393
        N, d = mat["DataMat"].shape
        featureMat = mat["DataMat"][:, :-1]
        targetMat = mat["DataMat"][:, -1].reshape(-1, 1)
        X, T, Xt, Tt = div_dataset(featureMat, targetMat, test_perc=Test_num / N, saxis=0)
        return X.T, T.reshape(-1, 1).T, Xt.T, Tt.reshape(-1, 1).T



def rbf_(x, y, gamma=None, njobs=1):
    pairdist = sklearn.metrics.pairwise_distances(x, y) ** 2
    if gamma is None:
        gamma = (pairdist.sum() / pairdist.shape[1] / pairdist.shape[0]) ** (-1)
    return np.exp(-gamma * pairdist), gamma


def div_dataset(X, T, test_perc=.2, saxis=0, dtype=np.float64):
    N = X.shape[saxis]
    idx = np.random.permutation(N)
    n2 = int(round(test_perc * N))

    X_ = np.take(X, idx[n2:], axis=saxis).astype(dtype=dtype)
    T_ = np.take(T, idx[n2:], axis=saxis).astype(dtype=dtype)
    Xt = np.take(X, idx[:n2], axis=saxis).astype(dtype=dtype)
    Tt = np.take(T, idx[:n2], axis=saxis).astype(dtype=dtype)
    return X_, T_, Xt, Tt


def mean_pm_std(x, signi=3, latex=False):
    """
    Compute average and standard deviation of the values in x and write `mean +- std` into a string
    :param x: List of values
    :type x: List of numbers or 1d numpy arrays
    :param signi: number of significative digits
    :type signi: int
    :return: string containing mean +- std of x
    :rtype: String
    """

    pm_str = "$\pm$" if latex else "+-"
    return str(np.round(np.mean(x), signi)) + " " + pm_str + " " + str(np.round(np.std(x), signi))


def compute_accuracy(t, that):
    """
    Compute accuracy given that t is the target array
    :param t: one_hot QxN matrices, Q is the number of classes, N the number of samples
    :param that: QxN matrix, Q is the number of classes, N the number of samples
    :returns: Accuracy between 0 and 1
    """
    try:
        assert(t.shape == that.shape)
    except AssertionError:
        print("Shape inconsistency:", t.shape, that.shape)
        sys.exit(1)
    return np.sum((np.argmax(t, axis=0) - np.argmax(that, axis=0)) == 0) / t.shape[1]


def compute_nme(t, that):
    """
    Compute the Normalized Mean Error

    Parameters
    ----------
    t : numpy.ndarray, size (n_targets, n_samples)
    True target value

    that : numpy.ndarray, size (n_targets, n_samples)
    Predicted target value

    Returns
    -------
    Normalized Mean Error
    """
    try:
        assert(t.shape == that.shape)
    except AssertionError:
        print("Shape inconsistency:", t.shape, that.shape)
        sys.exit(1)
    return 20 * np.log10(np.linalg.norm(t - that, 'fro') / np.linalg.norm(t, 'fro'))


def nme(t, that):
    return compute_nme(t.T, that.T)


def normc(x, axis=0):
    """
    Normalize the square of columns to 1, keeps the sign
    :param x: Input array to normalize
    :type x: ndarray
    :return: Normalized array
    :rtype: ndarray
    """
    return np.multiply(np.sqrt(np.divide(np.square(x), np.sum(np.square(x), axis=axis).reshape(-1, 1))), np.sign(x))


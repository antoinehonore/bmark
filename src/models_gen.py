"""
Created on 09 Dec 2018

@author: anthon
"""

from tbox.ml.models.ELM import ELM
from tbox.ml.models.PLN import PLN
from tbox.ml.models.DKF import DKF
from tbox.ml.models.DKN import DKN
from tbox.ml.models.BLINE import BLINE

from .utils import nme, compute_nme


def models_gen(all_nnodes=1000, all_alphas=tuple([10 ** i for i in range(-5, 6)]),
               metric=nme,
               all_lmax=30):

    return [
        DKN(nnodes=all_nnodes, delta=50, alphas=all_alphas, lmax=all_lmax, metric=metric),
        DKF(lmax=all_lmax, nnodes=all_nnodes, delta=50, alphas=all_alphas, metric=metric),
        # RidgeCV(alphas=all_alphas),
        ELM(nnodes=all_nnodes, alphas=all_alphas, metric=metric),
        PLN(nmax=all_nnodes, delta_n=50, alphas=all_alphas, lmax=all_lmax, metric=metric),
        BLINE()
    ]


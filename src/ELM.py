"""
Created on 16 May 2018

@author: anthon
"""

import numpy as np
from sklearn.linear_model.ridge import RidgeCV as RLScv
from sklearn.metrics.regression import r2_score


class ELM(object):
    """
    Extreme Learning Machine.

    Parameters
    ----------
    metric : Callable, optional, default: r2_score
    Function to score prediction.

    alphas : tuple, optional, default: 10**-4...10**4
    Regularization parameters

    K : int, optional, default: 3
    Number of fold in cross-validation

    n : int, optional, default: 1000
    Number of random nodes in input layer.
    """

    def __init__(self, metric=r2_score, alphas=tuple([10**i for i in range(-10, 11)]), K=3, nnodes=1000):
        self.alphas = alphas
        self.K = K
        self.n = nnodes
        self.metric = metric
        self.R = None
        self.map_to_target = None

        self.status = "Initialized"
        self.name="ELM"

    def reset(self):
        self.__init__(alphas=self.alphas, K=self.K, nnodes=self.n)


    def nlinearity(self, x):
        return np.maximum(0, x)


    def fit(self, X, T):
        if self.status == "Fitted":
            self.reset()

        N, d = X.shape
        self.R = np.random.randn(d, self.n)
        self.B = np.random.randn(1, self.n)
        Z = X @ self.R + self.B
        Y = self.nlinearity(Z)

        rls = RLScv(alphas=self.alphas, store_cv_values=True, fit_intercept=True)
        rls.fit(Y, T)
        self.alpha = rls.alpha_
        self.map_to_target = rls

        self.status = "Fitted"
        return self


    def predict(self, X):
        """
        Predict targets from feature vectors.

        Parameters
        ----------
        X : numpy.ndarray, size (n_samples, n_features)
        Feature vector to predict targets of.

        Returns
        -------
        that : numpy.ndarray, size (n_samples, n_targets)
        Matrix of predicted targets.
        """
        Y = self.nlinearity(X @ self.R + self.B)
        that = self.map_to_target.predict(Y)
        return that


    def score(self, X, T):
        """
        Scores feature vectors prediction vs. feature vector target

        Parameters
        ----------
        X : numpy.ndarray, size (n_samples, n_features)
        Feature vector to score the predict targets of.

        T : numpy.ndarray, size (n_samples, n_targets)
        Target vectors associated with x.

        Returns
        -------
        scores : numpy.ndarray (n_samples,)
        Scoring of the predictions of input feature vectors.
        """
        that = self.predict(X)
        score = self.metric(T, that)
        return score

if __name__ == "__main__":
    from sklearn.datasets import load_digits
    from tbox.ml.utils import compute_accuracy, makeOnehot
    digits = load_digits()
    n, d = digits.data.shape
    idx = np.random.permutation(n)
    test_perc = .3
    ntest = int(round(n * test_perc))
    X = digits.data[idx[ntest:], :]
    T = makeOnehot(digits.target[idx[ntest:], ...])
    Xt = digits.data[idx[:ntest], :]
    Tt = makeOnehot(digits.target[idx[:ntest], ...])

    model = ELM()
    model.fit(X, T)

    print(compute_accuracy(T.T, model.predict(X).T),
          compute_accuracy(Tt.T, model.predict(Xt).T) )

    print('')
"""
Created on 16 May 2018

@author: anthon
"""


import numpy as np
import sys
from .utils import normc, rbf_
from sklearn.metrics.scorer import r2_score


class DKN(object):
    """
    Dense Kernel Network.

    Parameters
    ----------
    nnodes : int, optional, default: 1000
    Number of random nodes at each layer.

    lmax : int, optional, default: 10
    Maximum number of layers.

    alphas : tuple, optional, default: 10^-2..10^3
    Regularization parameters of the kernel regressions.

    K : int, optional, default: 3
    Number of folds used for cross-validation.
    If K == 1 : The cross-validation is skipped and the first element of the alphas tuple is used at each layer.

    metric : Callable, optional, default: sklearn.metric.scorer.r2_score
    Scoring function.

    Attributes
    ----------
    map_to_target : list of numpy.ndarray
    Matrices used for mapping to target space.

    fulltrain_kern : list of numpy.ndarray
    Training data kernel at each layer.

    input_fulltrain : list of numpy.ndarray
    Store the training data input at each layer. This is to be able to compute the data kernel at prediction time.

    total_number_layer : int
    Increase by 1 each time a layer is added to the network.

    userdata : dictionary
    Stores information about the model: name, architecture, regression parameter.

    status : str
        'Initialized' : Is set after initialization
        'Converged' : Is set after fitting is done. If status == 'Converged' and the method fit() is called, the model
            is reinitialized before fitting.

    R1 : numpy.ndarray, size (d_features, nnodes)
    Input layer random matrix.

    R : list, length lmax - 1
    Hidden layers random matrix.

    VQ : numpy.ndarray, size (2 * n_targets, n_targets)
    Target estimator transformation, inherited from PLN.

    """

    def __init__(self, alphas=tuple([10**i for i in range(-5, 6)]), metric=r2_score, nnodes=1000,\
                 lmax=10, K=3, delta=50):

        self.nnodes = nnodes
        self.lmax = lmax
        self.alphas = alphas
        self.metric = metric
        self.delta = delta

        #Keep data in memory for prediction
        self.map_to_target = [0 for _ in range(lmax)]
        self.fulltrain_kern = [0 for _ in range(lmax)]
        self.input_fulltrain = [0 for _ in range(lmax)]

        self.train_data = None
        self.train_label = None
        self.train_perf = [0 for _ in range(self.lmax)]

        self.A = [None for _ in range(self.lmax)]
        self.y = [None for _ in range(self.lmax)]
        self.K = [None for _ in range(self.lmax)]
        self.gamma = [0 for _ in range(self.lmax)]


        self.tval_hat = np.zeros((self.lmax, len(self.alphas)))
        metric_str = str(metric)

        if "r2_score" in metric_str or "accuracy" in metric_str:
            self.choice_fun = np.argmax
        elif "nme" in metric_str:
            self.choice_fun = np.argmin
        else:
            print("unknown metric function. Exit.", metric)
            sys.exit(1)
        self.name = "DKN"
        self.total_number_layers = 0


    def improvement(self, old, new):
        if self.choice_fun == np.argmin:
            return new < old

        if self.choice_fun == np.argmax:
            return new > old



    def fit(self, X, T, vperc=.15):
        """
        Fit the model to the data in argument.

        Parameters
        ----------
        X : numpy.ndarray size (n_samples, n_features)
        Training data matrix.

        T : numpy.ndarray size (n_samples, n_targets)
        Training target matrix.

        """

        N, Q = T.shape
        _, d = X.shape

        self.VQ = np.concatenate((np.eye(Q), -np.eye(Q)), axis=0)
        self.alpha = [0 for _ in range(self.lmax)]


        N = X.shape[0]
        nval = int(round(N * vperc))
        permut = np.random.permutation(N)
        val_idx = permut[:nval]
        train_idx = permut[nval:]


        self.train_data = X
        self.train_label = T

        y_in = self.train_data
        K_in, gamma = self.kernel(self.train_data, self.train_data)



        self.gamma_init = gamma

        # Kval_in = self.kernel(self.train_data, xval)
        # yval_in = xval

        self.val_perf = np.zeros(self.lmax)
        self.train_perf = np.zeros_like(self.val_perf)

        for l in range(self.lmax):
            layer_val_perf = np.zeros(len(self.alphas))
            layer_train_perf = np.zeros_like(layer_val_perf)

            self.K[l] = K_in


            #   1-fold cross validation
            Kval = self.K[l][train_idx, :][:, val_idx]
            Ktrain = self.K[l][train_idx, :][:, train_idx]

            for ialpha, alpha in enumerate(self.alphas):
                tval_hat = Kval.T @ self.solve_regression(Ktrain, T[train_idx, :], alpha)
                ttrain_hat = Ktrain.T @ self.solve_regression(Ktrain, T[train_idx, :], alpha)
                layer_val_perf[ialpha] = self.metric(T[val_idx, :], tval_hat)
                layer_train_perf[ialpha] = self.metric(T[train_idx, :], ttrain_hat)


            ibest = self.choice_fun(layer_val_perf)
            self.val_perf[l] = layer_val_perf[ibest]
            self.alpha[l] = self.alphas[ibest]


            if l == 0 or self.improvement(self.val_perf[l-1], self.val_perf[l]):   #  continues
                #   calculate ttrain_hat for propagation
                ttrain_hat = self.K[l].T @ self.solve_regression(self.K[l], T, self.alpha[l])
                self.train_perf[l] = self.metric(T, ttrain_hat)

                #  tmp = self.score(self.train_data, self.train_label)

                self.A[l] = np.random.randn(y_in.shape[1], self.nnodes + self.delta * l)


                y_in = self.pln(y_in, self.A[l], ttrain_hat, self.VQ)
                self.y[l] = np.copy(y_in)

                Ktmp, gamma = self.kernel(y_in, y_in)

                self.gamma[l] = gamma

                K_in = Ktmp + K_in

            else:
                self.y[l-1], self.A[l-1], self.K[l] = None, None, None
                break
        return self


    def solve_regression(self, K, T, alpha):
        n = K.shape[0]
        return np.linalg.inv(K + alpha * n * np.eye(n)) @ T


    def predict(self, x):
        """
        Predict targets from feature vectors.

        Parameters
        ----------
        x : numpy.ndarray, size (n_samples, n_features)
        Feature vector to predict targets of.

        Returns
        -------
        that : numpy.ndarray, size (n_samples, n_targets)
        Matrix of predicted targets.
        """


        input_ = x
        data_kern = self.kernel(self.train_data, input_, gamma=self.gamma_init)
        for l in range(self.lmax):
            if self.A[l] is None:
                that = data_kern.T @ self.solve_regression(self.K[l], self.train_label, self.alpha[l])
                break

            that = data_kern.T @ self.solve_regression(self.K[l], self.train_label, self.alpha[l])
            input_ = self.pln(input_, self.A[l], that, self.VQ)
            data_kern = self.kernel(self.y[l], input_, gamma=self.gamma[l]) + data_kern

        return that


    #   Kernel computation
    def kernel(self, x, y, gamma=None):

        K, gamma_out = rbf_(x, y, gamma=gamma)
        if gamma is None:
            return K, gamma_out
        else:
            return K


    def score(self, x, t):
        """
        Scores feature vectors prediction vs. feature vector target

        Parameters
        ----------
        x : numpy.ndarray, size (n_samples, n_features)
        Feature vector to score the predict targets of.

        t : numpy.ndarray, size (n_samples, n_targets)
        Target vectors associated with x.

        Returns
        -------
        scores : numpy.ndarray (n_samples,)
        Scoring of the predictions of input feature vectors.
        """
        that = self.predict(x)
        return self.metric(t, that)


    def normalize(self, x):
        #return x
        return normc(x, axis=1)


    def pln(self, x, R, that, VQ):
        Z = np.concatenate((that @ VQ.T, self.normalize(x @ R)), axis=1)
        Y = np.maximum(0, Z)# + .1 * np.minimum(0, Z)
        return Y


import numpy as np
import sys

from bmark.src.utils import normc, div_dataset
from sklearn.linear_model import RidgeCV as RLScv
from sklearn.metrics.regression import r2_score


class PLN(object):
    """
    Progressive Learning Network.

    Parameters
    ----------
    metric : Callable
        Metric to use to measure the goodness of fit.

    alphas : tuple, optional, default tuple([10**i for i in range(-5, 6, 1)])
        L2 penalty (regularization term) parameter.

    K : int, optional, default 3
        Number of folds in the kfold crossvalidation

    tol_n : float, optional, default 0.0001
        Minimal increase in goodness of fit after additional random nodes.

    tol_l : float, optional, default 0.0001
        Threshold for the increase in goodness of fit after additional layer.

    delta_n : int, optional, default 100
        Additional random nodes increment parameter.

    nmax : int, optional, default 1000
        Maximum number of nodes per layer.

    lmax : int, optional, default 100
        Maximum number of layers.

    Attributes
    ---------
    d : int
        Input dimension.

    Q : int
        Target dimension.

    norm_fun : Callable
        Current normalization function (see paper).

    rls_model : sklearn.linear_model.RidgeCV object
        First map to target space.

    nlayers : int
        Current number of layer in the network.

    r : list of numpy.ndarray
        List of random matrix.

    VQ : numpy.ndarray, size (2*Q, Q)
        See paper

    target_space_maps : List, length nlayers
        Keeps one map to the target space per layer.

    nnodes : list, length nlayers
        Number of random nodes per layer

    score_ : list
        All goodness of fit attained after additional of random nodes.

    new_layer_score : list, length nlayers
        All goodness of fit attained after additional layers.


    References
    ---------
    @Paper:          Progressive Learning for Systematic Design of Large Neural Network
    @author:         Saikat Chatterjee, Alireza M. Javid, Mostafa Sadeghi, Partha P. Mitra, Mikael Skoglund
    @organization:   KTH, Royal Institute of Technology
    @contact:        PLN in Matlab: Alireza M. Javid, almj@kth.se, PLN in Python: Antoine Honoré, antoinehonor@gmail.com
    @website:        www.ee.kth.se/reproducible/

    Implementation in Python of the Progressive Learning training algorithm.
    This version is inspired by the implementation of Alireza M. Javid in Matlab.

    Notes
    -----
    ADMM algorithm has been replaced with scikit-learn implementation of cross-validated ridge regression.

    """
    def __init__(self, metric=r2_score, alphas=tuple([10**i for i in range(-5, 6, 1)]),\
                 K=3, tol_n=.001, tol_l=.001, delta_n=750, nmax=1000, lmax=100):

        self.alphas = alphas  # Regularization parameters prior
        self.K = K  # Number of k folds
        self.tol_n = tol_n  # Tolerance on performance increase after nodes addition
        self.tol_l = tol_l  # Tolerance on performance increase after layer addition
        self.nmax = nmax  # Maximum number of nodes per layers
        self.delta_n = delta_n  # Increment of number of nodes
        self.lmax = lmax  # Maximum number of layers
        self.metric = metric
        self.name = "PLN"

        metric_str = str(metric)
        if "r2_score" in metric_str or "accuracy" in metric_str:
            self.choice_fun = np.argmax
        elif "nme" in metric_str:
            self.choice_fun = np.argmin
        else:
            print("unknown metric function. Exit.", metric)
            sys.exit(1)

    def improvement(self, old, new):
        if self.choice_fun == np.argmin:
            return new < old

        if self.choice_fun == np.argmax:
            return new > old

    def normalize(self, x):
        return normc(x, axis=1)

    def predict(self, x, full_return=False):
        """
        :param x:  must be a matrix of size (input_size, n)
        :return: that, prediction, ndarray of size (Q,n)
        #        Y, input of the final O_i matrix (that=O_i * Y)
        """
        # Performs a forward pass of x on the PLN
        # input: x
        # Output:

        that = self.rls_model.predict(x)

        for ilayer in range(self.nlayers):
            m = x @ self.r[ilayer]

            # NORMALIZATION step
            m = self.normalize(m)

            Z = np.hstack((that @ self.VQ.T, m))

            # PP holding non linearity
            Y = self.ppholding_nl(Z)

            that = self.target_space_maps[ilayer].predict(Y)
            x = Y

        if full_return:
            return that, Y
        else:
            return that

    def ppholding_nl(self, x):
        return np.maximum(0, x)# + .1 * np.minimum(0, x)

    def score(self, x, t):
        that = self.predict(x)
        return self.metric(t, that)

    def fit(self, X, Y):
        n_samples, d = X.shape
        _, Q = Y.shape

        self.d = d
        self.Q = Q

        xtrain, ttrain, xval, tval = div_dataset(X, Y, test_perc=.15, saxis=0)

        # Initialize
        self.target_space_maps = [[] for _ in range(self.lmax)]  # List of (O_i) matrices (i is the index of the corresponding layer)
        self.r = []  # List of R_i matrices (i is the index of the corresponding layer)
        self.nnodes = [d]  # Stores the number of random nodes in the network
        self.VQ = np.vstack(
            (np.identity(Q), -np.identity(Q)))  # VQ must be chosen according to the PP holding no linearity (here relu)

        self.nlayers = 0  # Keeps track of the number of layer
        self.train_perf = []  # Store error achieved after addition of delta nodes
        self.val_perf = []  # Store error achieved after optimization of a new layer
        self.r = [0 for _ in range(self.lmax)]

        converged = False

        while not converged and self.nlayers < self.lmax:
            self.optimize_new_layer(xtrain, ttrain)
            self.val_perf.append(self.score(xval, tval))

            if (self.nlayers > 1 and not self.improvement(self.val_perf[-2], self.val_perf[-1])):
                self.nlayers -= 1
                converged = True

        return self


    def optimize_new_layer(self, x, t):
        # Optimizes a new layer on top of the existing network.
        train_score = np.zeros((2,))

        if self.nlayers == 0:  # Learn the Wls
            x_in = x
            ridgecv = RLScv(alphas=self.alphas)
            ridgecv.fit(x, t)
            self.rls_model = ridgecv
            that = self.rls_model.predict(x)
        else:
            that, x_in = self.predict(x, full_return=True)

        _, p = x_in.shape
        nnodes = min(max(x_in.shape[1], self.nmax) + self.delta_n * self.nlayers, self.nmax)
        self.r[self.nlayers] = np.random.randn(x_in.shape[1], nnodes )
        m = x_in @ self.r[self.nlayers]

        # Normalization
        m = self.normalize(m)

        # progression
        Z = np.hstack((that @ self.VQ.T, m))

        # Non-linearity
        Y = self.ppholding_nl(Z)

        ridgecv = RLScv(alphas=self.alphas, fit_intercept=True)
        ridgecv.fit(Y, t)
        self.target_space_maps[self.nlayers] = ridgecv

        that_internal = self.target_space_maps[self.nlayers].predict(Y)
        self.train_perf.append(self.metric(t, that_internal))
        self.nlayers += 1


if __name__ == "__main__":
    from sklearn.datasets import load_digits
    from tbox.ml.utils import compute_accuracy, makeOnehot
    digits = load_digits()
    n, d = digits.data.shape
    idx = np.random.permutation(n)
    test_perc = .3
    ntest = int(round(n * test_perc))
    X = digits.data[idx[ntest:], :]
    T = makeOnehot(digits.target[idx[ntest:], ...])
    Xt = digits.data[idx[:ntest], :]
    Tt = makeOnehot(digits.target[idx[:ntest], ...])

    model = PLN()
    model.fit(X, T)

    print(compute_accuracy(T.T, model.predict(X).T),
          compute_accuracy(Tt.T, model.predict(Xt).T) )

    print('')

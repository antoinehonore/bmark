#/bin/bash
SHELL=/bin/bash

SRCDIR := src
BINDIR := bin
TMPDIR := interm
RESDIR := results

########################################################################
# Default parameters

ifndef MODELS
    MODELS=ELM
endif

ifndef DS
    DS=Vowel
endif

ifndef n
    n=10
endif
########################################################################


interms = $(shell echo $(TMPDIR)/$(DS)_$(MODELS)_{1..$(n)}.interm)
targets = $(shell echo $(RESDIR)/$(DS)_final.txt)
srcs = $(shell echo $(SRCDIR)/$(MODELS).py)

all: 
	echo $(interms)

interm: $(interms)
final: $(targets)

$(RESDIR)/%_final.txt: $(interms) 
	python $(BINDIR)/make_final.py $@ $^

$(TMPDIR)/%.interm: $(BINDIR)/run.py
	python $< $@

.PRECIOUS: $(TMPDIR)/%.interm

clean:
	rm $(TMPDIR)/$(DS)_*.interm
	rm $(RESDIR)/$(DS)_final.txt

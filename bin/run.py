import os
import sys
import numpy as np
from datetime import datetime, timedelta
from bmark.src.utils import compute_accuracy, nme
from bmark.src.utils import load_dataset
from bmark.src.models_gen import models_gen
from tbox.utils import pidprint


def main(fname_target):
    print("passed arg:", fname_target, "...")
    fname_elts = os.path.basename(fname_target).split("_")
    dataset = fname_elts[0]
    model_name = fname_elts[1]
    # i_model = int(fname_elts[2].split('.')[0])
    print(">", fname_elts, dataset, model_name)
    pidprint(fname_target, "Loading...", dataset)
    X, T, Xt, Tt = load_dataset(fname=dataset)
    X_, T_, Xt_, Tt_ = X.T.astype(np.float64), T.T.astype(np.float64), Xt.T.astype(np.float64), Tt.T.astype(np.float64)

    pidprint(fname_target, "Data loaded.")

    models = models_gen(metric=nme)
    names = [m.name for m in models_gen()]
    model = models[names.index(model_name)]

    # Run and time fit() method
    start_t = datetime.now()
    model.fit(X_, T_)
    elapsed = (datetime.now() - start_t).total_seconds()

    # Calculate Performance
    train_err = nme(T_, model.predict(X_))
    tthat = model.predict(Xt_)
    test_err = nme(Tt_, tthat)
    test_acc = compute_accuracy(Tt_.T, tthat.T) * 100

    # Write to output file
    with open(fname_target, "a") as f:
        print(model.name, " | ", train_err, "|", test_err, "|", test_acc, "|", elapsed, file=f)

    return 0


if __name__ == "__main__":
    print("In main...")
    usage = "python bin/run.py [dataset file] [target filename]\n"\
            "Example: python bin/run.py data/emp-age-clin-15_proc.pkl interm/emp-age-clin-15_proc_1.interm"
    main(sys.argv[1])
    sys.exit(0)

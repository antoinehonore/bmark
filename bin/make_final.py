'''
Created on 09 Dec 2018

@author: anthon
'''

import sys, os
import numpy as np
from bmark.src.utils import mean_pm_std


def parse_file(f):
    with open(f, "r") as fp:
        line = fp.read()
    return [x.strip() for x in line.split("|")]


def main(outfile, allfiles):
    results = [parse_file(f) for f in allfiles]
    results_a = np.array(results)

    all_models = np.unique(results_a[:, 0]).tolist()

    for imodel in range(len(all_models)):
        sub_a = results_a[results_a[:, 0] == all_models[imodel]]
        num_a = sub_a[:, 1:].astype(np.float)
        formatted_s = [mean_pm_std(x, latex=True) for x in num_a.astype(np.float).T.tolist()]
        with open(outfile, 'a') as f1:
            print(" | ".join(["Model", "train NME", "test NME", "test Acc", "runtime (s)"]), file=f1)
            print(all_models[imodel], "|", " & ".join(formatted_s), file=f1)
    return 0


if __name__ == "__main__":
    outfile = sys.argv[1]
    allfiles = sys.argv[2:]

    main(outfile, allfiles)


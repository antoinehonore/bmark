# Example
Run ELM and PLN algorithm 10 times each on Vowel dataset.
Create the final file with average performances.
Clean the intermediate and final file.
Use --dry-run flag to see what WOULD be run.

```
$ make interm DS=Vowel MODELS=\{ELM,PLN\} (--dry-run)
$ make final DS=Vowel MODELS=\{ELM,PLN\} (--dry-run)
$ make clean DS=Vowel (--dry-run)
```

# Implement extra algorithms
Create a new class in ```src/``` folder, see ```src/Template.py``` for minimal fields and methods.
Add your model to the ```src/models_gen``` function with the correct default parameters if needed.

#  New datasets
Place your file in the ```data/``` folder.
Perform the appropriate modifications in ```bin/main.py``` and ```src/utils.load_dataset()``` to get your data in the correct format.

# Run new dataset and new algorithm
Performs 10 runs of the new model on the new dataset and display the results in stdin
```
$ make final DS=new-data.txt MODELS=new-model
$ cat results/new-data_final.txt
new-model | ... | ...
```
